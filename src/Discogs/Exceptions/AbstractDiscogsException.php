<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 14:20
 */

namespace Anarchalien\Discogs\Exceptions;

/**
 * Class AbstractDiscogsException
 * @package Anarchalien\Discogs\Exceptions
 */
abstract class AbstractDiscogsException extends \Exception
{
    /**
     * String
     */
    const DISCOGS_EXCEPTION = 'Unknown Discogs Exception';

    /**
     * @var string
     */
    protected $message = AbstractDiscogsException::DISCOGS_EXCEPTION;
}