<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 05/06/18
 * Time: 22:11
 */

namespace Anarchalien\Discogs\Exceptions;

/**
 * Class AuthException
 * @package Anarchalien\Discogs\Exceptions
 */
class AuthException extends AbstractDiscogsException
{
    /**
     * string
     */
    const NOT_AUTHORISED = 'Could not authorise with Discogs API';

    const AUTH_CONFIG = 'Authorisation headers incorrect';
}