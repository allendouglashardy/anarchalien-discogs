<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 15:40
 */

namespace Anarchalien\Discogs\Exceptions;

/**
 * Class BadParameterException
 * @package Anarchalien\Discogs\Exceptions
 */
class BadParameterException extends AbstractDiscogsException
{
    /**
     * string
     */
    const BAD_PARAMETER = 'Invalid parameter(s)';

    /**
     * @var string
     */
    protected $message = BadParameterException::BAD_PARAMETER;
}