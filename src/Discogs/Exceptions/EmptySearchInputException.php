<?php

namespace Anarchalien\Discogs\Exceptions;

/**
 * Class EmptySearchInputException
 * @package Anarchalien\Discogs\Exceptions
 */
class EmptySearchInputException extends AbstractDiscogsException
{
    /**
     * string
     */
    const EMPTY_SEARCH_INPUT = 'Search cannot be empty';

    /**
     * @var string
     */
    protected $message = EmptySearchInputException::EMPTY_SEARCH_INPUT;
}