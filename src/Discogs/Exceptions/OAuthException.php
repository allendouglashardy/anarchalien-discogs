<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 05/06/18
 * Time: 22:38
 */

namespace Anarchalien\Discogs\Exceptions;

/**
 * Class OAuthException
 * @package Anarchalien\Discogs\Exceptions
 */
class OAuthException extends AuthException
{
    const NONCE_EXCEPTION = 'Nonce is not provided correctly';

    const METHOD_EXCEPTION = 'Method missing';
}