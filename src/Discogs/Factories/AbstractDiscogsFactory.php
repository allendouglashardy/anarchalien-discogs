<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 18:18
 */

namespace Anarchalien\Discogs\Factories;

use Anarchalien\Discogs\Interfaces\DiscogsFactoryCreateInterface;

/**
 * Class AbstractDiscogsFactory
 * @package Anarchalien\Discogs\Factories
 */
abstract class AbstractDiscogsFactory implements DiscogsFactoryCreateInterface
{
}