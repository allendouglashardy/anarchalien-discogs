<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 18:25
 */

namespace Anarchalien\Discogs\Factories;

/**
 * Class DiscogsParameterFactory
 * @package Anarchalien\Discogs\Factories
 */
class DiscogsParameterFactory extends AbstractDiscogsFactory
{
    /**
     * @return mixed
     */
    public static function create()
    {
        return [];
    }
}