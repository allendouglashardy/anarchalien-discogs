<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 18:30
 */

namespace Anarchalien\Discogs\Factories;

use Anarchalien\Discogs\Services\Parameters\DiscogsParameter;

/**
 * Class DiscogsSearchParameterFactory
 * @package Anarchalien\Discogs\Factories
 */
class DiscogsSearchParameterFactory extends DiscogsParameterFactory
{
    /**
     * @return array|mixed
     */
    public static function create()
    {
        return [
            (new DiscogsParameter('query')),
            (new DiscogsParameter('title')),
            (new DiscogsParameter('release_title')),
            (new DiscogsParameter('credit')),
            (new DiscogsParameter('artist')),
            (new DiscogsParameter('anv')),
            (new DiscogsParameter('label')),
            (new DiscogsParameter('genre')),
            (new DiscogsParameter('style')),
            (new DiscogsParameter('country')),
            (new DiscogsParameter('year')),
            (new DiscogsParameter('format')),
            (new DiscogsParameter('catno')),
            (new DiscogsParameter('barcode')),
            (new DiscogsParameter('track')),
            (new DiscogsParameter('submitter')),
            (new DiscogsParameter('contributor')),
        ];
    }
}