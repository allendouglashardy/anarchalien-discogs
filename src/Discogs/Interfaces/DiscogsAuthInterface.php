<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 05/06/18
 * Time: 22:06
 */

namespace Anarchalien\Discogs\Interfaces;

/**
 * Interface DiscogsAuthInterface
 * @package Anarchalien\Discogs\Interfaces
 */
interface DiscogsAuthInterface
{
    /**
     * @return DiscogsAuthInterface
     */
    public function authenticate() : DiscogsAuthInterface;

    /**
     * @param string $key
     * @return DiscogsAuthInterface
     */
    public function setKey(string $key) : DiscogsAuthInterface;

    /**
     * @param string $secret
     * @return DiscogsAuthInterface
     */
    public function setSecret(string $secret) : DiscogsAuthInterface;

    /**
     * @return bool
     */
    public function isAuthenticated() :bool;
}