<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 18:21
 */

namespace Anarchalien\Discogs\Interfaces;

/**
 * Interface DiscogsFactoryCreateInterface
 * @package Anarchalien\Discogs\Interfaces
 */
interface DiscogsFactoryCreateInterface
{
    /**
     * @return mixed
     */
    public static function create();
}