<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 05/06/18
 * Time: 22:31
 */

namespace Anarchalien\Discogs\Interfaces;

/**
 * Interface DiscogsOAuthInterface
 * @package Anarchalien\Discogs\Interfaces
 */
interface DiscogsOAuthInterface
{
    /**
     * @param string $nonce
     * @return DiscogsOAuthInterface
     */
    public function setNonce(string $nonce) :DiscogsOAuthInterface;

    /**
     * @param string $method
     * @return DiscogsOAuthInterface
     */
    public function setMethod(string $method='PLAINTEXT') :DiscogsOAuthInterface;

    /**
     * @param null|string $timestamp
     * @return DiscogsOAuthInterface
     */
    public function setTimestamp(?string $timestamp) :DiscogsOAuthInterface;

    /**
     * @param null|string $callback
     * @return DiscogsOAuthInterface
     */
    public function setCallback(?string $callback) :DiscogsOAuthInterface;

    /**
     * @return string
     */
    public function getNonce() :string;

    /**
     * @return string
     */
    public function getMethod() :string;

    /**
     * @return string
     */
    public function getTimestamp() :string;

    /**
     * @return string
     */
    public function getCallback() :string ;

    /**
     * @return DiscogsAuthInterface
     */
    public function authenticate() :DiscogsAuthInterface;
}