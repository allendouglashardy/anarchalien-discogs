<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 15:16
 */

namespace Anarchalien\Discogs\Interfaces;

/**
 * Interface DiscogsParameterInterface
 * @package Anarchalien\Discogs\Interfaces
 */
interface DiscogsParameterInterface extends DiscogsInterface
{
    /**
     * @param array $data
     * @return bool
     */
    public function isValid(array $data=[]): bool;

    /**
     * @return array
     */
    public function getParameters() :array;
}