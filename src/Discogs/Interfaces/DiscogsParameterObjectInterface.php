<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 15:31
 */

namespace Anarchalien\Discogs\Interfaces;

/**
 * Interface DiscogsParameterObjectInterface
 * @package Anarchalien\Discogs\Interfaces
 */
interface DiscogsParameterObjectInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $value
     * @return DiscogsParameterObjectInterface
     */
    public function setValue(string $value=''):DiscogsParameterObjectInterface;

    /**
     * @return string
     */
    public function getValue():string;
}