<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 14:33
 */

namespace Anarchalien\Discogs\Interfaces;

/**
 * Interface DiscogsRequestInterface
 * @package Anarchalien\Discogs\Interfaces
 */
interface DiscogsRequestInterface extends DiscogsInterface
{
    /**
     * @param String $method
     * @param String $request
     * @param array $params
     * @return mixed
     */
    public function request(String $method='GET',String $request='',array $params=[]);

    /**
     * @param string $endpoint
     * @return DiscogsRequestInterface
     */
    public function setEndpoint(string $endpoint): DiscogsRequestInterface;

    /**
     * @param string $method
     * @return DiscogsRequestInterface
     */
    public function setMethod(string $method='GET'): DiscogsRequestInterface;

    /**
     * @param array $payload
     * @return DiscogsRequestInterface
     */
    public function setPayload(array $payload=[]): DiscogsRequestInterface;

    /**
     * @return array
     */
    public function run() :array;
}