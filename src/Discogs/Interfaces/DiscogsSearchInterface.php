<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 14:58
 */

namespace Anarchalien\Discogs\Interfaces;

/**
 * Interface DiscogsSearchInterface
 * @package Anarchalien\Discogs\Interfaces
 */
interface DiscogsSearchInterface
{
    /**
     * @param array $data
     * @return array
     */
    public function search(array $data=[]):array;
}