<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 15:24
 */

namespace Anarchalien\Discogs\Interfaces;

/**
 * Interface ParameterListInterface
 * @package Anarchalien\Discogs\Interfaces
 */
interface ParameterListInterface
{
    /**
     * @return array
     */
    public function getParameters():array;
}