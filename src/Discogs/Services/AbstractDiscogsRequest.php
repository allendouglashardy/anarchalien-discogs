<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 14:35
 */

namespace Anarchalien\Discogs\Services;


use Anarchalien\Discogs\Interfaces\DiscogsRequestInterface;

/**
 * Class AbstractDiscogsRequest
 * @package Anarchalien\Discogs\Services
 */
abstract class AbstractDiscogsRequest implements DiscogsRequestInterface
{
    /**
     * @var string
     */
    protected $method;

    /**
     * @var string
     */
    protected $request;

    /**
     * @var array
     */
    protected $params;

    /**
     * @param String $method
     * @param String $request
     * @param array $params
     * @return mixed|void
     */
    public function request(String $method = 'GET', String $request = '', array $params = [])
    {
        // TODO: Implement request() method.
    }

    /**
     * @param string $endpoint
     * @return DiscogsRequestInterface
     */
    public function setEndpoint(string $endpoint): DiscogsRequestInterface
    {
        $this->request = $endpoint;

        return $this;
    }

    /**
     * @param string $method
     * @return DiscogsRequestInterface
     */
    public function setMethod(string $method = 'GET'): DiscogsRequestInterface
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @param array $payload
     * @return DiscogsRequestInterface
     */
    public function setPayload(array $payload = []): DiscogsRequestInterface
    {
        $this->params = $payload;

        return $this;
    }

    /**
     * @return array
     */
    public function run(): array
    {
        $result = $this->request(
            $this->method,
            $this->request,
            $this->params
        );

        return [];
    }
}