<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 12/06/18
 * Time: 19:59
 */

namespace Anarchalien\Discogs\Services\Auth;

use Anarchalien\Discogs\Interfaces\DiscogsAuthInterface;
/**
 * Class AbstractAuthRequest
 * @package Anarchalien\Discogs\Services
 */
abstract class AbstractAuthRequest
{
    /**
     * @var DiscogsAuthInterface
     */
    protected $auth;

    /**
     * AbstractAuthRequest constructor.
     * @param DiscogsAuthInterface $auth
     */
    public function __construct(DiscogsAuthInterface $auth)
    {
        $this->auth = $auth;
    }
}