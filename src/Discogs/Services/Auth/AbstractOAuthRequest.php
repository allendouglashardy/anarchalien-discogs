<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 12/06/18
 * Time: 20:02
 */

namespace Anarchalien\Discogs\Services\Auth;

use Anarchalien\Discogs\Interfaces\DiscogsOAuthInterface;

/**
 * Class AbstractOAuthRequest
 * @package Anarchalien\Discogs\Services\Auth
 */
abstract class AbstractOAuthRequest extends AbstractAuthRequest
{
    /**
     * AbstractOAuthRequest constructor.
     * @param DiscogsOAuthInterface $auth
     */
    public function __construct(DiscogsOAuthInterface $auth)
    {
        parent::__construct($auth);
    }
}