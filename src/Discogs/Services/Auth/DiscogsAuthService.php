<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 05/06/18
 * Time: 22:12
 */

namespace Anarchalien\Discogs\Services\Auth;


use Anarchalien\Discogs\Exceptions\AuthException;
use Anarchalien\Discogs\Interfaces\DiscogsAuthInterface;

/**
 * Class DiscogsAuthService
 * @package Anarchalien\Discogs\Services\Auth
 */
class DiscogsAuthService implements DiscogsAuthInterface
{
    /**
     * @var bool
     */
    protected $authenticated = false;

    /**
     * @var string
     */
    protected $key = '';

    /**
     * @var string
     */
    protected $secret='';

    /**
     * @return DiscogsAuthInterface
     * @throws AuthException
     */
    public function authenticate(): DiscogsAuthInterface
    {
        if($this->secret==''||$this->key==''){
            throw new AuthException(AuthException::AUTH_CONFIG);
        }

        return $this;
    }

    /**
     * @param string $key
     * @return DiscogsAuthInterface
     */
    public function setKey(string $key): DiscogsAuthInterface
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @param string $secret
     * @return DiscogsAuthInterface
     */
    public function setSecret(string $secret): DiscogsAuthInterface
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAuthenticated(): bool
    {
        return $this->authenticated;
    }
}