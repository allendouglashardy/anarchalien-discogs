<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 14:30
 */

namespace Anarchalien\Discogs\Services;

use Anarchalien\Discogs\Interfaces\DiscogsInterface;
use Anarchalien\Discogs\Interfaces\DiscogsSearchInterface;
use Anarchalien\Discogs\Exceptions\EmptySearchInputException;
use Anarchalien\Discogs\Services\Parameters\DiscogsSearchParameters;

/**
 * Class DiscogsSearchService
 * @package Anarchalien\Discogs\Services
 */
class DiscogsSearchService extends AbstractDiscogsRequest
    implements DiscogsInterface,
    DiscogsSearchInterface
{

    /**
     * string
     */
    const ENDPOINT = '/database/search';

    /**
     * @var DiscogsSearchParameters
     */
    protected $searchParams;

    /**
     * DiscogsSearchService constructor.
     * @param DiscogsSearchParameters $parameters
     */
    public function __construct(
        DiscogsSearchParameters $parameters
    )
    {
        $this->searchParams = $parameters;
    }

    /**
     * @param array $data
     * @return array
     * @throws EmptySearchInputException
     * @throws \Anarchalien\Discogs\Exceptions\BadParameterException
     */
    public function search(array $data=[]) :array
    {
        $return = [];

        if(empty($data)){
            throw new EmptySearchInputException();
        }
        elseif ($this->searchParams->isValid($data) == true){
            $return =$this->setEndpoint(DiscogsSearchService::ENDPOINT)
                ->setMethod()
                ->setPayload()
                ->run();
        }

        return $return;
    }
}