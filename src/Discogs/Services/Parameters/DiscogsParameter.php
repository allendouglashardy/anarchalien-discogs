<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 15:30
 */

namespace Anarchalien\Discogs\Services\Parameters;

use Anarchalien\Discogs\Interfaces\DiscogsParameterObjectInterface;

/**
 * Class DiscogsParameter
 * @package Anarchalien\Discogs\Services\Parameters
 */
class DiscogsParameter implements DiscogsParameterObjectInterface
{
    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $value= '';

    /**
     * DiscogsParameter constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $value
     * @return DiscogsParameterObjectInterface
     */
    public function setValue(string $value=''): DiscogsParameterObjectInterface
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}