<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 15:18
 */

namespace Anarchalien\Discogs\Services\Parameters;

use Anarchalien\Discogs\Exceptions\BadParameterException;
use Anarchalien\Discogs\Interfaces\DiscogsParameterInterface;
use Anarchalien\Discogs\Interfaces\ParameterListInterface;

/**
 * Class DiscogsSearchParameters
 * @package Anarchalien\Discogs\Services\Parameters
 */
class DiscogsSearchParameters implements DiscogsParameterInterface
{
    /**
     * @var ParameterListInterface
     */
    protected $parameterList;

    /**
     * DiscogsSearchParameters constructor.
     * @param ParameterListInterface $parameterList
     */
    public function __construct(ParameterListInterface $parameterList)
    {
        $this->parameterList = $parameterList;
    }

    /**
     * @param array $data
     * @return bool
     * @throws BadParameterException
     */
    public function isValid(array $data = []): bool
    {
        if(empty($data) || $this->hasRequiredParameters($data) == false){
            return false;
        }

        return true;
    }

    /**
     * @param array $data
     * @return bool
     * @throws BadParameterException
     */
    protected function hasRequiredParameters($data=[]):bool
    {
        $params = [];

        $list = $this->parameterList->getParameters();

        foreach ($list as $parameter){
            if($parameter instanceof DiscogsParameter && in_array(
                $parameter->getName(),
                array_keys($data))
            ){
                $params[] = $parameter;
            }
        }

        if(empty($params)){
            throw new BadParameterException();
        }

        return true;
    }

    /**
     * @return array
     */
    public function getParameters():array
    {
        return $this->parameterList->getParameters();
    }
}