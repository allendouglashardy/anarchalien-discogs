<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 18:46
 */

namespace Anarchalien\Discogs\Services\Parameters;


use Anarchalien\Discogs\Interfaces\ParameterListInterface;
use Anarchalien\Discogs\Factories\DiscogsSearchParameterFactory;
/**
 * Class DiscorgsSearchParameterList
 * @package Anarchalien\Discogs\Services\Parameters
 */
class DiscorgsSearchParameterList implements ParameterListInterface
{
    /**
     * @var array|mixed
     */
    protected $parameters;

    /**
     * DiscorgsSearchParameterList constructor.
     */
    public function __construct()
    {
        $this->parameters = DiscogsSearchParameterFactory::create();
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }
}