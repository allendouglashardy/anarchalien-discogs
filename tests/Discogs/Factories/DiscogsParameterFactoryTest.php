<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 19:14
 */

use Anarchalien\Discogs\Factories\DiscogsParameterFactory;

/**
 * Class DiscogsParameterFactoryTest
 */
class DiscogsParameterFactoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     *
     */
    public function testCreateReturnsArray()
    {
        $result = DiscogsParameterFactory::create();

        $this->assertTrue(is_array($result));
    }
}