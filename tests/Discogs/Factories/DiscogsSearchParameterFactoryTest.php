<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 19:23
 */

use Anarchalien\Discogs\Factories\DiscogsSearchParameterFactory;
use Anarchalien\Discogs\Services\Parameters\DiscogsParameter;

/**
 * Class DiscogsSearchParameterFactoryTest
 */
class DiscogsSearchParameterFactoryTest extends \PHPUnit\Framework\TestCase
{
    protected $factory;

    protected $params=[
        'query',
        'type',
        'title',
        'release_title',
        'credit',
        'artist',
        'anv',
        'label',
        'genre',
        'style',
        'country',
        'year',
        'format',
        'catno',
        'barcode',
        'track',
        'submitter',
        'contributor'
    ];

    protected function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();

        $this->factory = DiscogsSearchParameterFactory::create();
    }

    public function testCreateReturnsArray()
    {
        $this->assertTrue(is_array($this->factory));
    }

    public function testResultContainsInstances()
    {
        foreach ($this->factory as $item){
            $this->assertInstanceOf(DiscogsParameter::class,$item);
        }
    }

    public function testParameterContent()
    {
        foreach ($this->factory as $item){
            $this->assertArrayHasKey($item->getName(),array_flip($this->params));
        }
    }
}