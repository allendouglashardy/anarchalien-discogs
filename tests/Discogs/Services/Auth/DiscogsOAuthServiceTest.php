<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 12/06/18
 * Time: 09:53
 */
use PHPUnit\Framework\TestCase;
use Anarchalien\Discogs\Services\Auth\DiscogsOAuthService;
use Anarchalien\Discogs\Interfaces\DiscogsOAuthInterface;
use Anarchalien\Discogs\Exceptions\OAuthException;

/**
 * Class DiscogsOAuthServiceTest
 */
class DiscogsOAuthServiceTest extends TestCase
{
    /**
     * @var DiscogsOAuthInterface $service
     */
    protected $service;

    protected function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        $this->service = (new DiscogsOAuthService());
    }

    public function testInstanceSetNonce()
    {
        $this->assertInstanceOf(DiscogsOAuthInterface::class,
            $this->service->setNonce('foo')
        );
    }

    public function testInstanceSetMethod()
    {
        $this->assertInstanceOf(
            DiscogsOAuthInterface::class,
            $this->service->setMethod('foo')
        );
    }

    public function testSetTimestampReturnsInterface()
    {
        $this->assertInstanceOf(
            DiscogsOAuthInterface::class,
            $this->service->setTimestamp('foo')
        );
    }

    public function testSetCallbackReturnsInstance()
    {
        $this->assertInstanceOf(
            DiscogsOAuthInterface::class,
            $this->service->setCallback('foo')
        );
    }

    public function testSetNonceReturnsSameValue()
    {
        $this->assertEquals(
          'foo',
          $this->service->setNonce('foo')->getNonce()
        );
    }

    public function testSetMethodReturnsMethod()
    {
        $this->assertEquals(
            'foo',
            $this->service->setMethod('foo')->getMethod()
        );
    }

    public function testSiteTimestampReturnsTimestamp()
    {
        $this->assertEquals(
            'foo',
            $this->service->setTimestamp('foo')->getTimestamp()
        );
    }

    public function testSetCallbackReturnsCallback()
    {
        $this->assertEquals(
            'foo',
            $this->service->setCallback('foo')->getCallback()
        );
    }

    public function testMissingNonceThrowsExceptionOnAuthenticate()
    {
        $this->expectException(OAuthException::class);
        $this->expectExceptionMessage(OAuthException::NONCE_EXCEPTION);

        $this->service->setMethod('foo')->setNonce('')->authenticate();
    }

    public function testMissingMethodThrowsExceptionOnAuthenticate()
    {
        $this->expectException(OAuthException::class);
        $this->expectExceptionMessage(OAuthException::METHOD_EXCEPTION);

        $this->service->setMethod('')->setNonce('foo')->authenticate();
    }
}