<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 14:49
 */

use Anarchalien\Discogs\Services\DiscogsSearchService;
use Anarchalien\Discogs\Services\Parameters\DiscogsSearchParameters;
use Anarchalien\Discogs\Services\Parameters\DiscorgsSearchParameterList;
/**
 * Class DiscogsSearchServiceTest
 */
class DiscogsSearchServiceTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var DiscogsSearchService
     */
    protected $discogsSearchService;

    public function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        $this->discogsSearchService = (
            new DiscogsSearchService(
                new DiscogsSearchParameters(
                    new DiscorgsSearchParameterList()
                )
            )
        );

        parent::setUp();
    }


    public function testSearchReturnsArray()
    {
        $result = $this->discogsSearchService->search(['x']);

        $this->assertTrue(is_array($result));
    }

    /**
     *
     */
    public function testEmptySearchException()
    {
        $this->expectException(\Anarchalien\Discogs\Exceptions\EmptySearchInputException::class);

        $this->discogsSearchService->search([]);
    }

    public function tearDown()/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::tearDown();
    }
}