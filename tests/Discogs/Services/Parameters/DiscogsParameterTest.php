<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 19:45
 */

use Anarchalien\Discogs\Services\Parameters\DiscogsParameter;

class DiscogsParameterTest extends \PHPUnit\Framework\TestCase
{
    public function testSetGetName()
    {
        $param = (new DiscogsParameter('foo'))->getName();

        $this->assertEquals('foo',$param);
    }

    public function testSetValueReturnsInstance()
    {
        $param = (new DiscogsParameter('foo'))->setValue('bar');

        $this->assertInstanceOf(DiscogsParameter::class,$param);
    }

    public function testSetGetValue()
    {
        $param = (new DiscogsParameter('foo'))->setValue('bar')->getValue();

        $this->assertEquals('bar',$param);
    }
}