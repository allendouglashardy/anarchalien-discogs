<?php
/**
 * Created by PhpStorm.
 * User: allen
 * Date: 27/05/18
 * Time: 20:10
 */
use Anarchalien\Discogs\Services\Parameters\DiscogsSearchParameters;
use Anarchalien\Discogs\Services\Parameters\DiscorgsSearchParameterList;
use Anarchalien\Discogs\Exceptions\BadParameterException;

/**
 * Class DiscogsSearchParametersTest
 */
class DiscogsSearchParametersTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var DiscogsSearchParameters
     */
    protected $searchParameters;

    protected function setUp()/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();

        $this->searchParameters = (
          new DiscogsSearchParameters(
              new DiscorgsSearchParameterList()
          )
        );
    }

    public function testIsValidBadParams()
    {
        $data = ['foo'=>'bar'];

        $this->expectException(BadParameterException::class);

        $this->searchParameters->isValid($data);
    }

    public function testFalseIsValid()
    {
        $this->assertFalse($this->searchParameters->isValid([]));
    }



    public function testIsValidGoodParams()
    {
        $data = ['query'=>'nirvana'];

        $this->assertTrue($this->searchParameters->isValid($data));
    }

    public function testGetParametersIsArray()
    {
        $result = $this->searchParameters->getParameters();

        $this->assertTrue(is_array($result));
    }
}